python-aedir - Python module for accessing Æ-DIR
================================================

This module contains:

1. wrapper class AEDirObject based on ldap0.ReconnectLDAPObject
2. several utility functions
3. boiler-plate code for tool processes

Official web site: [www.ae-dir.com](https://www.ae-dir.com/python.html)

License
-------

Copyright 2016-2018 Michael Ströder <michael@stroeder.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this module except in compliance with the License.
You may obtain a copy of the License at

    https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Example
-------

