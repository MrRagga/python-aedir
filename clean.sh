#!/bin/sh

python setup.py clean --all
rm -r .tox MANIFEST dist/* build/* *.egg-info
rm aedir/*.py? tests/*.py? *.py?
