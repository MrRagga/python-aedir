# -*- coding: utf-8 -*-
"""
Automatic tests for module package aedir

See https://www.ae-dir.com/python.html for details.
"""

import test_aedir_simple_functions
import test_aedirobject
