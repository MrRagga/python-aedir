# -*- coding: utf-8 -*-
"""
Automatic unit tests for class aedir.AEDirObject
"""

import os
import unittest

from ldap0.ldapobject import NO_UNIQUE_ENTRY
from ldap0.dn import explode_dn

from aedir.test import AETest

class Test01AEDirObject(AETest):
    """
    test class aedir.AEDirObject
    """
    inventory_path = 'tests/single-provider.json'
    j2_template_dir = os.path.join(
        os.environ['AEDIR_ROLE_DIR'],
        'templates',
        'slapd',
    )
    init_ldif_file = 'tests/ae-dir-init.ldif'
    ae_suffix = 'ou=ae-dir'
    maxDiff = 10000

    def test001_find_search_base(self):
        with self._get_conn() as aedir_conn:
            self.assertEqual(aedir_conn.find_search_base(), self.ae_suffix)

    def test002a_root_whoami_s(self):
        with self._get_conn() as aedir_conn:
            self.assertEqual(aedir_conn.whoami_s(), 'dn:cn=root,'+self.ae_suffix)

    def test002b_authzid_whoami_s(self):
        sasl_authz_id = 'dn:uid=msin,cn=ae,' + self.ae_suffix
        with self._get_conn(sasl_authz_id=sasl_authz_id) as aedir_conn:
            self.assertEqual(aedir_conn.whoami_s(), sasl_authz_id)
        with self._get_conn(sasl_authz_id='u:msin') as aedir_conn:
            self.assertEqual(aedir_conn.whoami_s(), sasl_authz_id)

    def test003_find_uid(self):
        with self._get_conn() as aedir_conn:
            msin = aedir_conn.find_uid('msin')
            self.assertEqual(
                msin,
                (
                    'uid=msin,cn=ae,'+self.ae_suffix,
                    {
                        'aeTicketId': ['INIT-42'],
                        'displayName': ['Michael Str\xc3\xb6der (msin/30000)'],
                        'description': ['initial \xc3\x86-DIR admin'],
                        'objectClass': [
                            'account', 'person', 'organizationalPerson',
                            'inetOrgPerson', 'aeObject', 'aeUser',
                            'posixAccount', 'ldapPublicKey'
                        ],
                        'loginShell': ['/bin/bash'],
                        'aeStatus': ['0'],
                        'gidNumber': ['30000'],
                        'givenName': ['Michael'],
                        'sn': ['Str\xc3\xb6der'],
                        'homeDirectory': ['/home/msin'],
                        'uid': ['msin'],
                        'mail': ['michael@stroeder.com'],
                        'uidNumber': ['30000'],
                        'aeTag': ['ae-tag-init', 'pub-tag-no-welcome-yet'],
                        'aePerson': ['uniqueIdentifier=INIT-PERSON-ID-42,cn=people,ou=ae-dir'],
                        'cn': ['Michael Str\xc3\xb6der']
                    },
                ),
            )
            msin = aedir_conn.find_uid('msin', attrlist=['uid', 'cn', 'memberOf'])
            self.assertEqual(
                msin,
                (
                    'uid=msin,cn=ae,'+self.ae_suffix,
                    {
                        'uid': ['msin'],
                        'cn': ['Michael Str\xc3\xb6der'],
                        'memberOf': ['cn=ae-admins,cn=ae,ou=ae-dir'],
                    },
                ),
            )


    def test004_get_user_groups(self):
        with self._get_conn() as aedir_conn:
            msin_groups = aedir_conn.get_user_groups('msin')
            self.assertEqual(
                msin_groups,
                set(['cn=ae-admins,cn=ae,ou=ae-dir']),
            )

    def test005a_full_simple_bind(self):
        with self._get_conn(
            who='uid=bccb,cn=test,ou=ae-dir',
            cred='Geheimer123456',
        ) as aedir_conn:
            self.assertEqual(aedir_conn.whoami_s(), 'dn:uid=bccb,cn=test,'+self.ae_suffix)
            self.assertEqual(aedir_conn.get_whoami_dn(), 'uid=bccb,cn=test,'+self.ae_suffix)

    def test005b_short_simple_bind(self):
        with self._get_conn(
            who='uid=bccb,ou=ae-dir',
            cred='Geheimer123456',
        ) as aedir_conn:
            self.assertEqual(aedir_conn.whoami_s(), 'dn:uid=bccb,cn=test,'+self.ae_suffix)
            self.assertEqual(aedir_conn.get_whoami_dn(), 'uid=bccb,cn=test,'+self.ae_suffix)

    def test005c_anon_bind(self):
        with self._get_conn(
            who='',
            cred='',
        ) as aedir_conn:
            self.assertEqual(aedir_conn.whoami_s(), '')
            self.assertEqual(aedir_conn.get_whoami_dn(), '')

    def test006_get_zoneadmins(self):
        with self._get_conn() as aedir_conn:
            zone_admins = aedir_conn.get_zoneadmins(
                'cn=test,ou=ae-dir',
                attrlist=['mail', 'cn'],
            )
            self.assertEqual(
                sorted(zone_admins),
                [
                    (
                        'uid=bccb,cn=test,ou=ae-dir',
                        {
                            'mail': ['michael@stroeder.com'],
                            'cn': ['Michael Str\xc3\xb6der']
                        }
                    )
                ],
            )

    def test007_find_aehost(self):
        with self._get_conn() as aedir_conn:
            aehost_dn, aehost_entry = aedir_conn.find_aehost('foo.example.com')
            self.assertEqual(
                (aehost_dn, aehost_entry),
                (
                    'host=foo.example.com,cn=test-services-1,cn=test,'+self.ae_suffix,
                    {
                        'aeNotBefore': ['20170116213536Z'],
                        'aeSrvGroup': ['cn=test-services-2,cn=test,ou=ae-dir'],
                        'aeStatus': ['0'],
                        'cn': ['foo'],
                        'host': ['foo.example.com'],
                        'objectClass': ['device', 'aeDevice', 'aeObject', 'aeHost', 'ldapPublicKey'],
                    },
                ),
            )

    def test008_get_service_groups(self):
        with self._get_conn() as aedir_conn:
            aehost_dn = 'host=foo.example.com,cn=test-services-1,cn=test,'+self.ae_suffix
            ae_srv_groups = aedir_conn.get_service_groups(aehost_dn)
            self.assertEqual(
                sorted(ae_srv_groups),
                [
                    (','.join(explode_dn(aehost_dn)[1:]), {}),
                    ('cn=test-services-2,cn=test,ou=ae-dir', {}),
                ]
            )
            ae_srv_groups = aedir_conn.get_service_groups(aehost_dn, attrlist=['cn'])
            self.assertEqual(
                sorted(ae_srv_groups),
                [
                    (','.join(explode_dn(aehost_dn)[1:]), {'cn': ['test-services-1']}),
                    ('cn=test-services-2,cn=test,ou=ae-dir', {'cn': ['test-services-2']}),
                ]
            )

    def test010_get_service_groups(self):
        with self._get_conn() as aedir_conn:
            aehost_dn = 'host=foo.example.com,cn=test-services-1,cn=test,' + self.ae_suffix
            srv_groups = aedir_conn.get_service_groups(aehost_dn)
            self.assertEqual(
                sorted(srv_groups),
                [
                    ('cn=test-services-1,cn=test,ou=ae-dir', {}),
                    ('cn=test-services-2,cn=test,ou=ae-dir', {}),
                ]
            )
            srv_groups = aedir_conn.get_service_groups(aehost_dn, attrlist=['cn'])
            self.assertEqual(
                sorted(srv_groups),
                [
                    ('cn=test-services-1,cn=test,ou=ae-dir', {'cn': ['test-services-1']}),
                    ('cn=test-services-2,cn=test,ou=ae-dir', {'cn': ['test-services-2']}),
                ]
            )
            srv_groups = aedir_conn.get_service_groups(aehost_dn, filterstr='(cn=*-2)')
            self.assertEqual(
                sorted(srv_groups),
                [
                    ('cn=test-services-2,cn=test,ou=ae-dir', {}),
                ]
            )

    def test011_get_user_srvgroup_relations(self):
        with self._get_conn() as aedir_conn:
            aesrvgroup_dn = 'cn=test-services-1,cn=test,' + self.ae_suffix
            srv_rels = aedir_conn.get_user_srvgroup_relations('bccb', aesrvgroup_dn)
            self.assertEqual(sorted(srv_rels), ['aeVisibleGroups'])
            srv_rels = aedir_conn.get_user_srvgroup_relations('bccb', aesrvgroup_dn, ['aeLoginGroups'])
            self.assertEqual(sorted(srv_rels), [])
            srv_rels = aedir_conn.get_user_srvgroup_relations('wxrl', aesrvgroup_dn)
            self.assertEqual(sorted(srv_rels), ['aeLoginGroups', 'aeVisibleGroups'])
            srv_rels = aedir_conn.get_user_srvgroup_relations('wxrl', aesrvgroup_dn, ['aeLoginGroups'])
            self.assertEqual(sorted(srv_rels), ['aeLoginGroups'])

    def test012_get_user_service_relations(self):
        with self._get_conn() as aedir_conn:
            aehost_dn = 'host=foo.example.com,cn=test-services-1,cn=test,' + self.ae_suffix
            srv_rels = aedir_conn.get_user_service_relations('bccb', aehost_dn)
            self.assertEqual(srv_rels, set(['aeLoginGroups', 'aeVisibleGroups']))
            srv_rels = aedir_conn.get_user_service_relations('bccb', aehost_dn, ['aeLoginGroups'])
            self.assertEqual(srv_rels, set(['aeLoginGroups']))
            srv_rels = aedir_conn.get_user_service_relations('wxrl', aehost_dn)
            self.assertEqual(srv_rels, set(['aeLoginGroups', 'aeVisibleGroups']))
            srv_rels = aedir_conn.get_user_service_relations('wxrl', aehost_dn, ['aeLoginGroups'])
            self.assertEqual(srv_rels, set(['aeLoginGroups']))

    def test013_get_users(self):
        with self._get_conn() as aedir_conn:
            aehost_dn = 'host=foo.example.com,cn=test-services-1,cn=test,' + self.ae_suffix
            users = aedir_conn.get_users(aehost_dn, attrlist=['uid', 'cn', 'uidNumber', 'memberOf'])
            self.assertEqual(
                users,
                (
                    101,
                    [
                        (
                            'uid=bccb,cn=test,ou=ae-dir',
                            {
                                'cn': ['Michael Str\xc3\xb6der'],
                                'uid': ['bccb'],
                                'uidNumber': ['30022'],
                                'memberOf': ['cn=test-zone-admins,cn=test,ou=ae-dir', 'cn=test-users-2,cn=test,ou=ae-dir'],
                            }
                        ),
                        (
                            'uid=luua,cn=test,ou=ae-dir',
                            {
                                'cn': ['Michael Str\xc3\xb6der'],
                                'uid': ['luua'],
                                'uidNumber': ['30025'],
                                'memberOf': ['cn=test-users-2,cn=test,ou=ae-dir'],
                            }
                        ),
                        (
                            'uid=wxrl,cn=test,ou=ae-dir',
                            {
                                'cn': ['Anna Blume'],
                                'uid': ['wxrl'],
                                'uidNumber': ['30027'],
                                'memberOf': ['cn=test-login-users-1,cn=test,ou=ae-dir'],
                            }
                        ),
                     ],
                    5,
                    [],
                    None,
                    None
                )
            )

    def test014_add_aeuser(self):
        with self._get_conn() as aedir_conn:
            uid = 'noob'
            new_entry = aedir_conn.add_aeuser('test', 'noob', 'uniqueIdentifier=web2ldap-1489425020.31,cn=people,ou=ae-dir')
            self.assertEqual(new_entry['uid'][0], uid)

    def test015_get_role_groups(self):
        with self._get_conn() as aedir_conn:
            role_groups = aedir_conn.get_role_groups(
                'host=foo.example.com,cn=test-services-1,cn=test,ou=ae-dir',
                ('aeVisibleGroups',),
            )
            self.assertEqual(
                role_groups,
                 {
                    'aeVisibleGroups': set([
                        'cn=test-users-2,cn=test,ou=ae-dir',
                        'cn=test-login-users-1,cn=test,ou=ae-dir',
                    ])
                 },
            )

    def test016_get_role_groups_filter(self):
        with self._get_conn() as aedir_conn:
            role_groups_filter = aedir_conn.get_role_groups_filter(
                'host=foo.example.com,cn=test-services-1,cn=test,ou=ae-dir',
                'memberOf',
                'aeVisibleGroups',
            )
            self.assertEqual(
                role_groups_filter,
                '(|(memberOf=cn=test-login-users-1,cn=test,ou=ae-dir)(memberOf=cn=test-users-2,cn=test,ou=ae-dir))',
            )

    def test017_get_sudoers(self):
        with self._get_conn() as aedir_conn:
            self.assertEqual(
                aedir_conn.get_sudoers(
                    'host=foo.example.com,cn=test-services-1,cn=test,ou=ae-dir',
                ),
                [],
            )
            self.assertEqual(
                aedir_conn.get_sudoers(
                    'host=ae-dir-deb-p1.virtnet1.stroeder.local,cn=ae-dir-provider-hosts,cn=ae,ou=ae-dir',
                    attrlist=None,
                ),
                [
                    (
                        'cn=ae-sudo-sys-admins,cn=ae,ou=ae-dir',
                        {
                            'cn': ['ae-sudo-sys-admins'],
                            'description': ['su - root for AE-DIR system admins'],
                            'sudoCommand': ['ALL'],
                            'sudoUser': ['%ae-sys-admins'],
                            'objectClass': ['top', 'sudoRole', 'aeObject', 'aeSudoRule'],
                            'sudoHost': ['ALL'],
                            'sudoRunAsUser': ['ALL'],
                        }
                    )
                ],
            )

    def test018_set_password(self):
        with self._get_conn() as aedir_conn:
            host_dn = aedir_conn.set_password(
                'foo.example.com',
                'secretpw',
            )
            self.assertEqual(
                host_dn,
                ('host=foo.example.com,cn=test-services-1,cn=test,ou=ae-dir', None),
            )
            with self._get_conn(who='host=foo.example.com,cn=test-services-1,cn=test,ou=ae-dir', cred='secretpw') as ldap_conn:
                self.assertEqual(
                    ldap_conn.whoami_s(),
                    'dn:host=foo.example.com,cn=test-services-1,cn=test,ou=ae-dir',
                )
            with self.assertRaises(NO_UNIQUE_ENTRY):
                aedir_conn.set_password(
                    'foobar_does_not_exist',
                    'secretpw',
                )
            with self.assertRaises(NO_UNIQUE_ENTRY):
                aedir_conn.set_password(
                    'foobar',
                    'secretpw',
                    # provoke non-unique name to DN mapping
                    filterstr_tmpl='(|(uid=msin)(uid=ae-dir-pwd))',
                )

    def test019_find_aegroup(self):
        with self._get_conn() as aedir_conn:
            aegroup_dn, aegroup_entry = aedir_conn.find_aegroup('test-users-2')
            self.assertEqual(
                (aegroup_dn, aegroup_entry),
                (
                    'cn=test-users-2,cn=test,'+self.ae_suffix,
                    {
                        'aeMemberZone': ['cn=test,ou=ae-dir'],
                        'aeNotBefore': ['20170116213310Z'],
                        'aeStatus': ['0'],
                        'cn': ['test-users-2'],
                        'description': ['Test group #2'],
                        'gidNumber': ['30024'],
                        'member': ['uid=bccb,cn=test,ou=ae-dir', 'uid=luua,cn=test,ou=ae-dir'],
                        'memberUid': ['bccb', 'luua'],
                        'objectClass': ['top', 'groupOfEntries', 'posixGroup', 'aeObject', 'aeGroup'],
                    },
                ),
            )

    def test019_add_aehost(self):
        with self._get_conn() as aedir_conn:
            aedir_conn.add_aehost('foo2.example.net', 'test-services-2', password='secret123')
            aehost_dn, aehost_entry = aedir_conn.find_aehost('foo2.example.net')
            del aehost_entry['userPassword']
            self.assertEqual(
                (aehost_dn, aehost_entry),
                (
                    'host=foo2.example.net,cn=test-services-2,cn=test,'+self.ae_suffix,
                    {
                        'aeStatus': ['0'],
                        'cn': ['foo2.example.net'],
                        'host': ['foo2.example.net'],
                        'objectClass': ['device', 'aeDevice', 'aeObject', 'aeHost', 'ldapPublicKey'],
                    },
                ),
            )
        with self._get_conn(who='host=foo2.example.net,cn=test-services-2,cn=test,'+self.ae_suffix, cred='secret123') as ldap_conn:
            self.assertEqual(
                ldap_conn.whoami_s(),
                'dn:host=foo2.example.net,cn=test-services-2,cn=test,'+self.ae_suffix,
            )


if __name__ == '__main__':
    unittest.main()
