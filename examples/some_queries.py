import sys

import aedir

AEDIR_URI = 'ldapi://%2Fhome%2Fmichael%2FProj%2Fae-dir%2Fopenldap%2Fldapi-p/????x-saslmech=EXTERNAL'

PYLDAP_TRACE_LEVEL = 1
PYLDAP_TRACE_FILE = sys.stdout

CACHE_TTL=20.0

# connection to AE-DIR provider
aedir_conn = aedir.AEDirObject(
    AEDIR_URI,
    trace_level=PYLDAP_TRACE_LEVEL,
    trace_file=PYLDAP_TRACE_FILE,
    cache_ttl=CACHE_TTL,
)

aeroot_dn = aedir_conn.find_search_base()

print "--------\nfind_search_base()", aeroot_dn

print "--------\nfind_uid('msin') ->", aedir_conn.find_uid('msin')

print "--------\nget_user_groups('msin') ->", aedir_conn.get_user_groups('msin')

print "--------\nget_zoneadmins('uid=bccb,cn=test,ou=ae-dir',attrlist=['mail', 'cn']) ->", aedir_conn.get_zoneadmins('uid=bccb,cn=test,ou=ae-dir',attrlist=['mail', 'cn'])

aehost_dn, _ = aedir_conn.find_aehost('foo.example.com')

print "--------\naehost_dn =", repr(aehost_dn)

# connection to AE-DIR consumer
aedir_conn = aedir.AEDirObject(
    AEDIR_URI,
    trace_level=PYLDAP_TRACE_LEVEL,
    trace_file=PYLDAP_TRACE_FILE,
    cache_ttl=CACHE_TTL,
)

print "--------\nfind_search_base()", aeroot_dn

aedir_conn.sasl_external_bind_s(authz_id='dn:'+aehost_dn)

aeroot_dn = aedir_conn.find_search_base()

print "--------\nget_service_groups(aehost_dn) ->", aedir_conn.get_service_groups(aehost_dn)

print "--------\naedir_conn.get_users(aehost_dn, attrlist=['uid', 'memberOf']) ->", aedir_conn.get_users(aehost_dn, attrlist=['uid', 'memberOf'])

print "--------\naedir_conn.get_users(aehost_dn, attrlist=['uid', 'memberOf'], ref_attr='aeLoginGroups') ->", aedir_conn.get_users(aehost_dn, attrlist=['uid', 'memberOf'], ref_attr='aeLoginGroups')

print "--------\nfind_uid('bccb', attrlist=['uid', 'memberOf']) ->", aedir_conn.find_uid('bccb', attrlist=['uid', 'memberOf'])

print "--------\nget_user_groups('bccb') ->", aedir_conn.get_user_groups('bccb')

print "--------\nget_user_service_relations('bccb', aehost_dn) ->", aedir_conn.get_user_service_relations('bccb', aehost_dn)

print "--------\nget_user_service_relations('bccb', aehost_dn, ['aeLoginGroups']) ->", aedir_conn.get_user_service_relations('bccb', aehost_dn, ['aeLoginGroups'])

print "--------\nget_cache_hit_ratio() ->", aedir_conn.get_cache_hit_ratio()
