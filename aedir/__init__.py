# -*- coding: utf-8 -*-
"""
aedir - Generic classes and functions for dealing with AE-DIR
"""

from __future__ import absolute_import

# Imports from Python's standard lib
import random
import time
import sys
import os
import string
import logging
import logging.handlers
import logging.config
import pprint

# Imports from ldap0 module package
import ldap0
import ldap0.sasl
import ldap0.controls
import ldap0.modlist
from ldap0.dn import escape_dn_chars
from ldap0.filter import escape_filter_chars, map_filter_parts, compose_filter
from ldap0.ldapobject import ReconnectLDAPObject
from ldap0.syncrepl import SyncreplConsumer
from ldap0.controls.readentry import PreReadControl, PostReadControl
from ldap0.controls.deref import DereferenceControl
from ldap0.ldapurl import LDAPUrl

from aedir.__about__ import __version__, __author__, __license__

# exported symbols
__all__ = [
    'AEDirUrl',
    'AEDirObject',
    'AEDIR_SEARCH_BASE',
    'aedir_aehost_dn',
    'aedir_aeuser_dn',
    'extract_zone',
    'LoggerFileObj',
    'parse_ldap_config_file',
    'random_string',
    'init_logger',
    'members2uids',
]

# Timeout in seconds when connecting to local and remote LDAP servers
# used for ldap0.OPT_NETWORK_TIMEOUT and ldap0.OPT_TIMEOUT
LDAP_TIMEOUT = 4.0

# Number of times connecting to LDAP is tried
LDAP_MAXRETRYCOUNT = 2
LDAP_RETRYDELAY = 2.0

# Default search base for AE-DIR
AEDIR_SEARCH_BASE = 'ou=ae-dir'

AESRVGROUP_GROUPREF_ATTRS = [
    'aeDisplayNameGroups',
    'aeSetupGroups',
    'aeLogStoreGroups',
    'aeLoginGroups',
    'aeVisibleGroups',
    'aeVisibleSudoers',
]

# filter template for locating any active entity by name which authenticates
# (e.g. with password)
AUTHC_ENTITY_FILTER_TMPL = (
    '(&'
      '(|'
        '(&'
          '(|(objectClass=aeUser)(objectClass=aeService))'
          '(uid={0})'
        ')'
        '(&'
          '(objectClass=aeHost)'
          '(host={0})'
        ')'
      ')'
      '(aeStatus=0)'
    ')'
)

# default length for generated passwords
PWD_LENGTH = 40

# alphabet used  for generated passwords
PWD_ALPHABET = string.letters + string.digits

# where to find the logging config file
AE_LOGGING_CONFIG = os.environ.get('AE_LOGGING_CONFIG', '/opt/ae-dir/etc/ae-logging.conf')
# which logger to get (qualname)
AE_LOGGER_QUALNAME = os.environ.get('AE_LOGGER_QUALNAME', 'aedir')


def extract_zone(ae_object_dn, aeroot_dn=AEDIR_SEARCH_BASE):
    """
    return the extracted zone name from dn
    """
    asserted_suffix = ','+aeroot_dn.lower()
    if not ae_object_dn.lower().endswith(asserted_suffix):
        raise ValueError(
            '%r does not end with %r' % (ae_object_dn, asserted_suffix)
        )
    return ldap0.dn.explode_dn(ae_object_dn[0:-len(aeroot_dn)-1])[-1][3:]


def aedir_aeuser_dn(uid, zone=None, aeroot_dn=AEDIR_SEARCH_BASE):
    """
    Returns a bind DN of a aeUser entry
    """
    if not zone:
        try:
            uid, zone = uid.split('@', 1)
        except ValueError:
            pass
    if zone:
        first_dn_part = ldap0.functions.escape_str(
            escape_dn_chars,
            'uid=%s,cn=%s',
            uid,
            zone,
        )
    else:
        first_dn_part = ldap0.functions.escape_str(
            escape_dn_chars,
            'uid=%s',
            uid,
        )
    return ','.join((first_dn_part, aeroot_dn))


def aedir_aehost_dn(fqdn, srvgrp=None, zone=None, aeroot_dn=AEDIR_SEARCH_BASE):
    """
    Returns a bind DN of a aeHost entry
    """
    if zone and srvgrp:
        first_dn_part = ldap0.functions.escape_str(
            escape_dn_chars,
            'host=%s,cn=%s,cn=%s',
            fqdn,
            srvgrp,
            zone
        )
    else:
        first_dn_part = ldap0.functions.escape_str(
            escape_dn_chars,
            'host=%s',
            fqdn
        )
    return ','.join((first_dn_part, aeroot_dn))


def aedir_aegroup_dn(aegroup_cn, aeroot_dn=AEDIR_SEARCH_BASE):
    """
    Returns a bind DN of a aeHost entry
    """
    zone_cn, _ = aegroup_cn.split('-', 1)
    return 'cn={0},cn={1},{2}'.format(
        aegroup_cn,
        zone_cn,
        aeroot_dn,
    )


class AEDirUrl(LDAPUrl):
    """
    LDAPUrl class for AE-DIR with some more LDAP URL extensions
    """
    attr2extype = {
        'who':'bindname',
        'cred':'X-BINDPW',
        'trace_level':'trace',
        'pwd_filename':'x-pwdfilename',
        'sasl_mech':'x-saslmech',
        'sasl_authzid':'x-saslauthzid',
    }

    def __init__(
            self,
            ldapUrl=None,
            urlscheme='ldap',
            hostport='',
            dn='',
            attrs=None,
            scope=None,
            filterstr=None,
            extensions=None,
            who=None,
            cred=None,
            trace_level=None,
            sasl_mech=None,
            pwd_filename=None,
        ):
        LDAPUrl.__init__(
            self,
            ldapUrl=ldapUrl,
            urlscheme=urlscheme,
            hostport=hostport,
            dn=dn,
            attrs=attrs,
            scope=scope,
            filterstr=filterstr,
            extensions=extensions,
            who=who,
            cred=cred,
        )
        self.trace_level = trace_level
        self.sasl_mech = sasl_mech
        self.pwd_filename = pwd_filename


class AEDirObject(
        ReconnectLDAPObject,
        SyncreplConsumer,
    ):
    """
    AE-DIR connection class
    """
    aeroot_filter = '(objectClass=aeRoot)'

    def __init__(
            self,
            ldap_url,
            trace_level=0,
            trace_file=None,
            trace_stack_limit=5,
            retry_max=LDAP_MAXRETRYCOUNT,
            retry_delay=LDAP_RETRYDELAY,
            timeout=LDAP_TIMEOUT,
            who=None,
            cred=None,
            cacert_filename=None,
            client_cert_filename=None,
            client_key_filename=None,
            cache_ttl=0.0,
            sasl_authz_id='',
        ):
        """
        Opens connection, sets some LDAP options and binds according
        to what's provided in `uri'.

        Extensions to parameters passed to ReconnectLDAPObject():
        `ldap_url'
          Can contain the full LDAP URL with authc information and base-DN
        `who'
          Bind-DN to be used (overrules ldap_url)
        `cred'
          Bind password to be used (overrules ldap_url)
        """
        # Parse/store LDAP URL into AEDirUrl instance
        if isinstance(ldap_url, unicode):
            ldap_url = ldap_url.encode('utf-8')
        if isinstance(ldap_url, str):
            self.ldap_url_obj = AEDirUrl(ldap_url)
        elif isinstance(ldap_url, AEDirUrl):
            self.ldap_url_obj = ldap_url
        elif ldap_url is None:
            self.ldap_url_obj = AEDirUrl(
                ldapUrl=ldap0.functions.get_option(ldap0.OPT_URI).split(' ')[0].strip() or None,
                dn=(ldap0.functions.get_option(ldap0.OPT_DEFBASE) or '').strip() or None,
            )
        else:
            raise ValueError('Invalid value for ldap_url: %r' % ldap_url)
        ReconnectLDAPObject.__init__(
            self,
            self.ldap_url_obj.initializeUrl(),
            trace_level=trace_level or int(self.ldap_url_obj.trace_level or '0'),
            trace_file=trace_file,
            trace_stack_limit=trace_stack_limit,
            cache_ttl=cache_ttl,
            retry_max=retry_max,
            retry_delay=retry_delay,
        )
        # Set TLS options
        if self.ldap_url_obj.urlscheme != 'ldapi':
            self.set_tls_options(
                cacert_filename=cacert_filename,
                client_cert_filename=client_cert_filename,
                client_key_filename=client_key_filename,
            )
        # Switch off automatic referral chasing
        self.set_option(ldap0.OPT_REFERRALS, 0)
        # Switch off automatic alias dereferencing
        self.set_option(ldap0.OPT_DEREF, ldap0.DEREF_NEVER)
        # Set timeout values
        self.set_option(ldap0.OPT_NETWORK_TIMEOUT, timeout)
        self.set_option(ldap0.OPT_TIMEOUT, timeout)
        # Send StartTLS extended operation if clear-text connection was initialized
        if self.ldap_url_obj.urlscheme == 'ldap':
            self.start_tls_s()
        # Bind with authc information found in ldap_url and key-word arguments
        if (self.ldap_url_obj.urlscheme == 'ldapi' and who is None) \
           or \
           (self.ldap_url_obj.sasl_mech and self.ldap_url_obj.sasl_mech.upper() == 'EXTERNAL'):
            sasl_authz_id = sasl_authz_id or self.ldap_url_obj.sasl_authzid or ''
            self.sasl_non_interactive_bind_s('EXTERNAL', authz_id=sasl_authz_id)
        else:
            if who is None:
                who = self.ldap_url_obj.who
            if cred is None:
                cred = self.ldap_url_obj.cred
            if who is not None:
                self.simple_bind_s(who, cred or '')
        # determine the Æ-DIR search base (aeRoot) and cache it
        self._aeroot_dn = None
        self.find_search_base()
        return # __init__()

    def find_search_base(self):
        """
        Returns the aeRoot entry
        """
        if self._aeroot_dn:
            return self._aeroot_dn
        # read namingContexts from rootDSE
        root_dse = self.read_rootdse_s(
            attrlist=['namingContexts', 'defaultNamingContext', 'aeRoot']
        )
        aeroot_dn = root_dse.get('aeRoot', root_dse.get('defaultNamingContext', [None]))[0]
        if aeroot_dn is not None:
            self._aeroot_dn = aeroot_dn
            return aeroot_dn
        naming_contexts = root_dse.get('namingContexts', [])
        while naming_contexts:
            # last element
            naming_context = naming_contexts.pop(-1)
            result = self.search_s(
                naming_context,
                ldap0.SCOPE_SUBTREE,
                self.aeroot_filter,
                attrlist=['1.1'],
                sizelimit=2,
            )
            if len(result) != 1:
                raise ldap0.NO_UNIQUE_ENTRY(
                    'No or non-unique search result for %r' % (self.aeroot_filter)
                )
            else:
                self._aeroot_dn = result[0][0]
                return result[0][0]
        raise ldap0.NO_UNIQUE_ENTRY(
            'aeRoot not found in %r' % (naming_contexts)
        )

    def find_byname(self, name, name_attr='cn', object_class='aeObject', attrlist=None):
        """
        Returns a unique aeGroup entry
        """
        return self.find_unique_entry(
            self._aeroot_dn,
            filterstr='(&(objectClass={oc})({at}={name}))'.format(
                oc=escape_filter_chars(object_class),
                at=escape_filter_chars(name_attr),
                name=escape_filter_chars(name),
            ),
            attrlist=attrlist,
        )

    def find_uid(self, uid, attrlist=None):
        """
        Returns a unique aeUser or aeService entry found by uid
        """
        return self.find_unique_entry(
            self._aeroot_dn,
            filterstr='(uid={uid})'.format(
                uid=escape_filter_chars(uid),
            ),
            attrlist=attrlist,
        )

    def find_aegroup(self, common_name, attrlist=None):
        """
        Returns a unique aeGroup entry
        """
        return self.find_byname(common_name, name_attr='cn', object_class='aeGroup', attrlist=attrlist)

    def find_aehost(self, host_name, attrlist=None):
        """
        Returns a unique aeHost entry found by attribute 'host'
        """
        return self.find_byname(host_name, name_attr='host', object_class='aeHost', attrlist=attrlist)

    def get_zoneadmins(self, ae_object_dn, attrlist=None, suppl_filter=''):
        """
        Returns LDAP search results of active aeUser entries of all
        zone-admins responsible for the given `ae_object_dn'.
        """
        zone_name = extract_zone(ae_object_dn, aeroot_dn=self._aeroot_dn)
        zone_entry = self.read_s(
            'cn={0},{1}'.format(zone_name, self._aeroot_dn),
            filterstr='(objectClass=aeZone)',
            attrlist=['aeZoneAdmins'],
        )
        if not (zone_entry and 'aeZoneAdmins' in zone_entry):
            return []
        member_of_filter = ''.join([
            '(memberOf={0})'.format(escape_filter_chars(group_dn))
            for group_dn in zone_entry.get('aeZoneAdmins', [])
        ])
        return self.search_s(
            self._aeroot_dn,
            ldap0.SCOPE_SUBTREE,
            filterstr=(
                '(&'
                '(objectClass=aeUser)'
                '(aeStatus=0)'
                '(|{0})'
                '{1}'
                ')'
            ).format(
                member_of_filter,
                suppl_filter,
            ),
            attrlist=attrlist,
        ) # get_zoneadmins()

    def get_user_groups(self, uid, memberof_attr=None):
        """
        Gets a set of DNs of aeGroup entries a AE-DIR user (aeUser or
        aeService) is member of
        """
        if memberof_attr:
            attrlist = [memberof_attr]
        else:
            attrlist = ['1.1']
        aeuser_dn, aeuser_entry = self.find_uid(uid, attrlist=attrlist)
        if memberof_attr in aeuser_entry:
            memberof = set([
                v.lower()
                for v in aeuser_entry[memberof_attr]
            ])
        else:
            ldap_result = self.search_s(
                self._aeroot_dn,
                ldap0.SCOPE_SUBTREE,
                ldap0.functions.escape_str(
                    escape_filter_chars,
                    '(&(objectClass=aeGroup)(member=%s))',
                    aeuser_dn
                ),
                attrlist=['1.1'],
            )
            memberof = set([
                dn.lower()
                for dn, _ in ldap_result
            ])
        return memberof # get_user_groups()

    def search_service_groups(
            self,
            service_dn,
            filterstr='', attrlist=None,
            serverctrls=None,
        ):
        """
        starts searching all service group entries the aeHost/aeService defined by
        service_dn is member of
        """
        aeservice_entry = self.read_s(
            service_dn,
            attrlist=['aeSrvGroup']
        ) or {}
        aesrvgroup_dn_list = [','.join(ldap0.dn.explode_dn(service_dn)[1:])]
        aesrvgroup_dn_list.extend(aeservice_entry.get('aeSrvGroup', []))
        srvgroup_filter = '(&{0}{1})'.format(
            compose_filter('|', map_filter_parts('entryDN', aesrvgroup_dn_list)),
            filterstr,
        )
        msg_id = self.search(
            self._aeroot_dn,
            ldap0.SCOPE_SUBTREE,
            srvgroup_filter,
            attrlist=attrlist or ['1.1'],
            serverctrls=serverctrls,
        )
        return msg_id

    def get_service_groups(self, service_dn, filterstr='', attrlist=None):
        """
        returns all service group entries the aeHost/aeService defined by
        service_dn is member of
        """
        msg_id = self.search_service_groups(service_dn, filterstr=filterstr, attrlist=attrlist)
        if msg_id is None:
            return []
        return self.result(msg_id)[1]

    def get_user_srvgroup_relations(
            self,
            uid,
            aesrvgroup_dn,
            ref_attrs=None
        ):
        """
        Gets relationship between a aeUser and a aeSrvGroup object by
        simply returning the matching attributes of the aeSrvGroup entry
        """
        aeuser_memberof = self.get_user_groups(uid)
        ref_attrs = ref_attrs or AESRVGROUP_GROUPREF_ATTRS
        aesrvgroup_entry = self.read_s(
            aesrvgroup_dn,
            filterstr='(objectClass=aeSrvGroup)',
            attrlist=ref_attrs
        )
        if not aesrvgroup_entry:
            raise ValueError('Empty search result for %r' % aesrvgroup_dn)
        srvgroup_relations = []
        for attr_type in ref_attrs:
            if attr_type in aesrvgroup_entry:
                for attr_value in aesrvgroup_entry[attr_type]:
                    if attr_value.lower() in aeuser_memberof:
                        srvgroup_relations.append(attr_type)
                        break
        return srvgroup_relations # get_user_srvgroup_relations()

    def get_user_service_relations(
            self,
            uid,
            service_dn,
            ref_attrs=None
        ):
        """
        get relation(s) between aeUser specified by uid with
        aeHost/aeService specified by service_dn

        returns set of relationship attribute names
        """
        aesrvgroups = self.get_service_groups(service_dn, attrlist=None)
        aesrvgroup_dn_set = set([
            aesrvgroup_dn
            for aesrvgroup_dn, _ in aesrvgroups
        ])
        result = set()
        for aesrvgroup_dn in aesrvgroup_dn_set:
            result.update(
                self.get_user_srvgroup_relations(
                    uid,
                    aesrvgroup_dn=aesrvgroup_dn,
                    ref_attrs=ref_attrs,
                )
            )
        return result

    def search_users(
            self,
            service_dn=None,
            ae_status=0,
            filterstr='(|(objectClass=aeUser)(objectClass=aeService))',
            attrlist=None,
            ref_attr='aeLoginGroups',
            serverctrls=None,
        ):
        """
        starts async search for all aeUser/aeService entries having the
        appropriate relationship on given aeHost/aeService
        """
        aesrvgroups = self.get_service_groups(
            service_dn,
            filterstr='({0}=*)'.format(ref_attr),
            attrlist=[ref_attr],
        )
        ref_attrs_groups = set()
        for _, srvgroup_entry in aesrvgroups:
            ref_attrs_groups.update(map(str.lower, srvgroup_entry.get(ref_attr, [])))
        if not ref_attrs_groups:
            return None
        user_group_filter = '(&(aeStatus={0}){1}(|{2}))'.format(
            str(int(ae_status)),
            filterstr,
            ''.join([
                '(memberOf={0})'.format(escape_filter_chars(dn))
                for dn in ref_attrs_groups
            ]),
        )
        msg_id = self.search(
            self._aeroot_dn,
            ldap0.SCOPE_SUBTREE,
            user_group_filter,
            attrlist=attrlist,
            serverctrls=serverctrls,
        )
        return msg_id

    def get_users(
            self,
            service_dn,
            ae_status=0,
            filterstr='(|(objectClass=aeUser)(objectClass=aeService))',
            attrlist=None,
            ref_attr='aeLoginGroups',
            serverctrls=None,
        ):
        """
        returns all aeUser/aeService entries having the appropriate
        relationship on given aeHost/aeService  as list of 2-tuples
        """
        msg_id = self.search_users(
            service_dn,
            ae_status=ae_status,
            filterstr=filterstr,
            attrlist=attrlist,
            ref_attr=ref_attr,
            serverctrls=serverctrls,
        )
        if msg_id is None:
            return []
        return self.result(msg_id)

    def get_next_id(self, id_pool_dn=None, id_pool_attr='gidNumber'):
        """
        consumes next ID by sending MOD_INCREMENT modify operation with
        pre-read entry control
        """
        id_pool_dn = id_pool_dn or self._aeroot_dn
        prc = PreReadControl(criticality=True, attrList=[id_pool_attr])
        msg_id = self.modify(
            id_pool_dn,
            [(ldap0.MOD_INCREMENT, id_pool_attr, '1')],
            serverctrls=[prc],
        )
        _, _, _, resp_ctrls, _, _ = self.result(msg_id)
        return int(resp_ctrls[0].entry[id_pool_attr][0])

    def add_aeuser(
            self,
            zone,
            uid,
            ae_person,
            ae_status=0,
            ae_ticket_id=None,
            description=None,
            aeperson_attrs=None,
            login_shell=None,
            home_directory='/home/{uid}',
            pwd_policy_subentry='cn=ppolicy-users,cn=ae,ou=ae-dir',
        ):
        """
        add a new aeUser entry beneath given zone
        """
        aeperson_attrs = aeperson_attrs or ['mail', 'sn', 'givenName']
        # read the associated aePerson entry
        ae_person_entry = self.read_s(
            ae_person,
            filterstr='(aeStatus=0)',
            attrlist=aeperson_attrs,
        )
        if not ae_person_entry:
            raise ldap0.NO_SUCH_OBJECT('no active and readable aePerson entry %r' % ae_person)
        # get new POSIX-UID/GID
        posix_id = str(self.get_next_id())
        # DN of new entry
        add_dn = aedir_aeuser_dn(uid, zone=zone, aeroot_dn=self._aeroot_dn)
        # generate modlist (entry's content)
        add_modlist = [
            ('objectClass', [
                'account',
                'person',
                'organizationalPerson',
                'inetOrgPerson',
                'aeObject',
                'aeUser',
                'posixAccount',
                'ldapPublicKey',
            ]),
            ('aeStatus', [str(ae_status)]),
            ('uid', [uid]),
            ('aePerson', [ae_person]),
            ('uidNumber', [posix_id]),
            ('gidNumber', [posix_id]),
            ('cn', [
                '{givenName} {sn}'.format(
                    givenName=ae_person_entry['givenName'][0],
                    sn=ae_person_entry['sn'][0],
                )
            ]),
            ('homeDirectory', [home_directory.format(uid=uid)]),
            ('displayName', [
                '{givenName} {sn} ({uid}/{uidNumber})'.format(
                    givenName=ae_person_entry['givenName'][0],
                    sn=ae_person_entry['sn'][0],
                    uid=uid,
                    uidNumber=posix_id,
                )
            ]),
            ('pwdPolicySubentry', [pwd_policy_subentry]),
        ]
        for attr_type, attr_values in ae_person_entry.items():
            add_modlist.append((attr_type, attr_values))
        for attr_type, attr_value in (
                ('aeTicketId', ae_ticket_id),
                ('description', description),
                ('loginShell', login_shell),
            ):
            if attr_value:
                add_modlist.append((attr_type, [attr_value]))
        msg_id = self.add(
            add_dn,
            add_modlist,
            serverctrls=[PostReadControl(
                criticality=True,
                attrList=['*', '+']
            )],
        )
        _, _, _, resp_ctrls, _, _ = self.result(msg_id)
        return resp_ctrls[0].entry

    def get_role_groups(self, service_dn, role_attrs):
        srv_grps = self.get_service_groups(service_dn, attrlist=role_attrs)
        role_groups = {}
        for role_attr in role_attrs:
            role_groups[role_attr] = set()
            for _, srv_grp_entry in srv_grps:
                role_groups[role_attr].update(srv_grp_entry.get(role_attr, []))
        return role_groups

    def get_role_groups_filter(
            self,
            service_dn,
            assertion_type,
            role_attr='aeVisibleGroups',
        ):
        groups = self.get_role_groups(service_dn, [role_attr])[role_attr]
        if groups:
            entry_dn_filter = ldap0.filter.compose_filter(
                '|',
                ldap0.filter.map_filter_parts(assertion_type, sorted(groups)),
            )
        else:
            entry_dn_filter = ''
        return entry_dn_filter

    def get_sudoers(
            self,
            service_dn,
            filterstr=None,
            attrlist=None,
            serverctrls=None,
        ):
        if attrlist is None:
            attrlist = [
                'cn', 'objectClass', 'description',
                'sudoCommand',
                'sudoHost',
                'sudoNotAfter', 'sudoNotBefore',
                'sudoOption', 'sudoOrder',
                'sudoRunAs', 'sudoRunAsGroup', 'sudoRunAsUser', 'sudoUser',
            ]
        if serverctrls is None:
            serverctrls = []
        serverctrls.append(DereferenceControl(True, {'aeVisibleSudoers': attrlist}))
        msg_id = self.search_service_groups(service_dn, attrlist=['1.1'], serverctrls=serverctrls)
        sudoers = []
        for _, res_data, _, _ in self.results(msg_id, add_ctrls=1):
            for dn, entry, controls in res_data:
                if controls and controls[0].controlType == DereferenceControl.controlType:
                    sudoers.extend(controls[0].derefRes['aeVisibleSudoers'])
        return sudoers

    def add_aehost(self, host_name, srvgroup_name, entry=None, password=None):
        """
        Add a new host entry for given `host_name' (FQDN) beneath aeSrvGroup
        defined by `srvgroup_name'
        """
        srvgroup_dn, _ = self.find_byname(
            srvgroup_name,
            name_attr='cn',
            object_class='aeSrvGroup',
            attrlist=['1.1'],
        )
        host_dn = ','.join((
            'host={0}'.format(escape_dn_chars(host_name)),
            srvgroup_dn,
        ))
        host_entry = {
            'objectClass': ['device', 'aeDevice', 'aeObject', 'aeHost', 'ldapPublicKey'],
            'cn': [host_name],
            'host': [host_name],
            'aeStatus': ['0'],
        }
        self.add_s(host_dn, host_entry)
        if entry:
            host_entry.update(entry)
        if password:
            self.passwd_s(host_dn, None, password)

    def set_password(self, name, password, filterstr_tmpl=AUTHC_ENTITY_FILTER_TMPL):
        """
        Set a password of an entity specified by name.
        The entity can be a aeUser, aeService or aeHost and its full DN
        is searched by unique find based on filterstr_tmpl.

        A 2-tuple with DN of the entry and result of LDAPObject.passwd_s()
        is returned as result.

        The caller has to handle exception ldap0.ldapobject.NO_UNIQUE_ENTRY.
        """
        ldap_filter = filterstr_tmpl.format(escape_filter_chars(name))
        dn, _ = self.find_unique_entry(
            self.find_search_base(),
            scope=ldap0.SCOPE_SUBTREE,
            filterstr=ldap_filter,
        )
        passwd_res = self.passwd_s(dn, None, password)
        return (dn, passwd_res)


def members2uids(members):
    """
    transforms list of group member DNs into list of uid values
    """
    return [
        dn[4:].split(',')[0]
        for dn in members
    ]


def init_logger(
        log_name=None,
        logging_config=AE_LOGGING_CONFIG,
        logger_qualname=AE_LOGGER_QUALNAME,
    ):
    """
    get logger instance
    """
    logging.config.fileConfig(logging_config)
    logger = logging.getLogger(logger_qualname)
    if log_name:
        logger.name = log_name
    return logger
