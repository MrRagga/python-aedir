# -*- coding: utf-8 -*-
"""
aedir.test - base classes for unit tests
"""

from __future__ import absolute_import

# from Python's standard lib
import json
import logging
import os

# from Jinja2
import jinja2

# from ldap0
from ldap0.test import SlapdObject, SlapdTestCase
from ldap0.logger import LoggerFileObj

# from aedir
from aedir import AEDirObject

__all__ = [
    'AESlapdObject',
    'AETest',
]


class AESlapdObject(SlapdObject):
    """
    run AE-DIR test slapd process
    """

    testrunsubdirs = (
        'schema',
        'um',
        'accesslog',
        'session',
    )
    openldap_schema_files = (
        'core.schema',
        'cosine.schema',
        'inetorgperson.schema',
        'dyngroup.schema',
        'openldap.schema',
        'ppolicy.schema',
        'nis.schema',
        'duaconf.schema',
    )

    def __init__(self, inventory_hostname, inventory, j2_template_dir):
        self._inventory = inventory
        self._inventory_local = self._inventory[inventory_hostname]
        self._openldap_role = self._inventory_local['openldap_role']
        self._j2_template_dir = j2_template_dir
        self._j2_template_filename = os.path.join(
            self._j2_template_dir,
            self._openldap_role+'.conf.j2',
        )
        SlapdObject.__init__(self)
        self._schema_prefix = os.path.join(self.testrundir, 'schema')
        self._oath_ldap_socket_path = os.path.join(self.testrundir, 'bind-listener')
        self._inventory_local.update({
            'oath_ldap_socket_path': self._oath_ldap_socket_path,
            'openldap_conf_prefix': self.testrundir,
            'aedir_etc_openldap': self.testrundir,
            'openldap_slapd_conf': self._slapd_conf,
            'openldap_data': self.testrundir,
            'openldap_rundir': self.testrundir,
            'openldap_schema_prefix': self._schema_prefix,
            'aedir_schema_prefix': self._schema_prefix,
            'openldap_server_id': self.server_id,
            'hostvars': self._inventory,
            'aedir_rootdn_uid_number': os.getuid(),
            'aedir_rootdn_gid_number': os.getgid(),
        })

    def setup_rundir(self):
        """
        creates rundir structure
        """
        SlapdObject.setup_rundir(self)
        self._ln_schema_files(
            self._inventory_local['openldap_schema_files'],
            os.path.join(
                os.environ['AEDIR_ROLE_DIR'],
                'files',
                'schema',
            ),
        )

    def gen_config(self):
        """
        generates a slapd.conf based on Jinja2 template
        and returns it as one string
        """
        # intialize template rendering
        jinja_env = jinja2.Environment(
            loader=jinja2.FileSystemLoader(self._j2_template_dir, encoding='utf-8'),
            trim_blocks=True,
            undefined=jinja2.StrictUndefined,
            autoescape=None,
        )
        # generate other files and write to disk
        for fdir, fname in (
                (self.testrundir, 'rootDSE.ldif'),
            ):
            jinja_template = jinja_env.get_template(fname+'.j2')
            config_filename = os.path.join(fdir, fname)
            self._log.debug('Write file %s', config_filename)
            with open(config_filename, 'wb') as cfile:
                cfile.write(jinja_template.render(self._inventory_local))
        # generate slapd.conf and return as result
        jinja_template = jinja_env.get_template(self._openldap_role+'.conf.j2')
        slapd_conf = jinja_template.render(self._inventory_local)
        return slapd_conf


class AETest(SlapdTestCase):
    """
    test class which initializes an AE-DIR slapd
    """

    server_class = AESlapdObject
    ldap_object_class = AEDirObject
    inventory_path = None
    j2_template_dir = None
    init_ldif_file = None
    pyldap_trace_level = int(os.environ.get('PYLDAP_TRACE_LEVEL', '0'))

    @classmethod
    def setUpClass(cls):
        # read inventory dict from JSON file
        with open(cls.inventory_path, 'rb') as json_file:
            cls.inventory = json.loads(json_file.read())
        # initialize dict of slapd instances and start them
        cls.servers = dict()
        if cls.j2_template_dir is None:
            raise ValueError('No directory specified for Jinja2 config templates!')
        if not os.path.exists(cls.j2_template_dir):
            raise ValueError('Directory %r specified for Jinja2 config templates does not exist!', cls.j2_template_dir)
        for inventory_hostname in cls.inventory.keys():
            server = cls.server_class(
                inventory_hostname,
                cls.inventory,
                cls.j2_template_dir,
            )
            server.start()
            # store server instance for later use
            cls.servers[inventory_hostname] = server
        # load LDIF file into first replica
        if cls.init_ldif_file:
            cls.servers.values()[0].ldapadd(
                None,
                extra_args=[
                    '-e', 'relax',
                    '-f', cls.init_ldif_file,
                ],
            )
        cls._rootdn_conn = {}
        # open a connection to each replica as rootdn by connecting via LDAPI and
        # binding with SASL/EXTERNAL
        for inventory_hostname, server in cls.servers.items():
            server._log.debug('Open LDAPI connection to %s', server.ldapi_uri)
            cls._rootdn_conn[inventory_hostname] = cls.ldap_object_class(
                server.ldapi_uri,
                trace_level = cls.pyldap_trace_level,
                trace_file = LoggerFileObj(server._log, logging.DEBUG),
            )
            server._log.info(
                'Opened LDAPI connection to %s as %s',
                server.ldapi_uri,
                cls._rootdn_conn[inventory_hostname].whoami_s()
            )

    def setUp(self):
        pass

    def _get_conn(
            self,
            inventory_hostname=None,
            who=None,
            cred=None,
            cacert_filename=None,
            client_cert_filename=None,
            client_key_filename=None,
            cache_ttl=0.0,
            sasl_authz_id='',
        ):
        if inventory_hostname:
            server = self.servers[inventory_hostname]
        else:
            server = self.servers.values()[0]
        aedir_conn = self.ldap_object_class(
            server.ldapi_uri,
            trace_level = self.pyldap_trace_level,
            trace_file = LoggerFileObj(server._log, logging.DEBUG),
            who=who,
            cred=cred,
            cacert_filename=cacert_filename,
            client_cert_filename=client_cert_filename,
            client_key_filename=client_key_filename,
            cache_ttl=cache_ttl,
            sasl_authz_id=sasl_authz_id,
        )
        return aedir_conn

    @classmethod
    def tearDownClass(cls):
        for server in cls.servers.values():
            server.stop()
