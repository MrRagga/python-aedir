# -*- coding: utf-8 -*-
"""
Basic boilerplate code for maintenance processes and tools etc.
"""

from __future__ import absolute_import

# Imports from Python's standard lib
import socket
import sys
import os
import time

# Imports from ldap0 module package
import ldap0
import ldap0.functions

import aedir

# exported symbols
__all__ = [
    'AEProcess',
    'TimestampStateMixin',
]

# Exception class used for catching all exceptions
CatchAllException = Exception

class AEProcess(object):
    """
    Base process class
    """
    initial_state = None
    script_version = '(no version)'
    ldap_url = None
    pyldap_tracelevel = 0

    def __init__(self):
        self.script_name = os.path.basename(sys.argv[0])
        self.host_fqdn = socket.getfqdn()
        self.logger = aedir.init_logger(self.script_name)
        self.logger.debug(
            'Starting %s %s on %s',
            sys.argv[0],
            self.script_version,
            self.host_fqdn
        )
        self.logger.debug('Connecting to %r...', self.ldap_url)
        self.ldap_conn = aedir.AEDirObject(self.ldap_url, trace_level=self.pyldap_tracelevel)
        self.logger.debug(
            'Conntected to %r bound as %r',
            self.ldap_conn.ldap_url_obj.initializeUrl(),
            self.ldap_conn.whoami_s(),
        )
        # not really started yet
        self.start_time = None
        self.run_counter = None
        # no SMTP connection yet
        self._smtp_conn = None

    def __enter__(self):
        return self

    def __exit__(self, *args):
        try:
            self.logger.debug(
                'Close LDAP connection to %r',
                self.ldap_conn.ldap_url_obj.initializeUrl()
            )
            self.ldap_conn.unbind_s()
        except ldap0.LDAPError:
            pass
        if self._smtp_conn is not None:
            self.logger.debug(
                'Close SMTP connection to %r',
                self._smtp_conn._remote_host
            )
            self._smtp_conn.quit()

    def _smtp_connection(
            self,
            url,
            local_hostname=None,
            tls_args=None,
            debug_level=0,
        ):
        """
        Open SMTP connection if not yet done before
        """
        # lazy import of optional dependency
        import mailutil
        if self._smtp_conn is not None:
            return self._smtp_conn
        local_hostname = local_hostname or self.host_fqdn
        self.logger.debug('Open SMTP connection to %r from %r', url, local_hostname)
        self._smtp_conn = mailutil.smtp_connection(
            url,
            local_hostname=local_hostname,
            tls_args=tls_args,
            debug_level=debug_level,
        )
        return self._smtp_conn

    def get_state(self):
        """
        get current state (to be overloaded by derived classes)
        """
        return self.initial_state

    def set_state(self, state):
        """
        set current state (to be overloaded by derived classes)
        """
        pass

    def run_worker(self, state):
        """
        one iteration of worker run (to be overloaded by derived classes)

        must return next state to be passed to set_state()
        """
        # this should never be called anyway => log warning
        self.logger.warn('Nothing done in %s.run_worker()', self.__class__.__name__)
        return None

    def exit(self):
        """
        method called on exit
        (to be overloaded by derived classes, e.g. for logging a summary)
        """
        return

    def run(self, max_runs=1, run_sleep=60.0):
        """
        the main program
        """
        self.start_time = time.time()
        self.run_counter = 0
        try:
            # first run
            self.set_state(self.run_worker(self.get_state()))
            self.run_counter += 1
            while self.run_counter < max_runs or max_runs is None:
                # further runs
                self.set_state(self.run_worker(self.get_state()))
                self.run_counter += 1
                time.sleep(run_sleep)
        except KeyboardInterrupt:
            self.logger.info('Exit on keyboard interrupt')
            self.exit()
        except Exception:
            self.logger.error(
                'Unhandled exception during processing request:',
                exc_info=True
            )
        else:
            self.exit()
        return # end of AEProcess.run()


class TimestampStateMixin(object):
    """
    Mix-in class for AEProcess which implements timestamp-based
    state strings in a file
    """
    initial_state = '19700101000000Z'

    def get_state(self):
        """
        Read the timestamp of last run from file `sync_state_filename'
        """
        try:
            with open(self.state_filename, 'rb') as file_obj:
                last_run_timestr = file_obj.read().strip() or self.initial_state
            # check syntax by parsing the string
            ldap0.functions.strp_secs(last_run_timestr)
        except CatchAllException, err:
            self.logger.warn(
                'Error reading timestamp from file %r: %s',
                self.state_filename,
                err
            )
            last_run_timestr = self.initial_state
        else:
            self.logger.debug(
                'Read last run timestamp %r from file %r',
                last_run_timestr,
                self.state_filename,
            )
        return last_run_timestr # end of get_state()

    def set_state(self, current_time_str):
        """
        Write the current state
        """
        if not current_time_str:
            current_time_str = self.initial_state
        try:
            # Write the last run timestamp
            with open(self.state_filename, 'wb') as file_obj:
                file_obj.write(current_time_str)
        except CatchAllException, err:
            self.logger.warn('Could not write %r: %s', self.state_filename, err)
        else:
            self.logger.debug('Wrote %r to %r', current_time_str, self.state_filename)
        return # end of set_state()
